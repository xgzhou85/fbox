/* 
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
  with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Neel Gala
Email id: neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package tb_float_i_to_f; 
  `include "Logger.bsv"
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;
  import fpu_convert :: * ;
  import RegFile :: * ;
  import fpu_common :: * ;

`ifdef FLEN64
  `define FLEN 64	
  `define a1 51
  `define a2 52
  `define a3 62
  `define a4 63
  `define MOD mk_fpu_int_to_dp
`else
  `define FLEN 32
  `define a1 22
  `define a2 23
  `define a3 30
  `define a4 31
  `define MOD mk_fpu_int_to_sp
`endif

  (*synthesize*)
  module mktb_float(Empty);
    RegFile#(Bit#(`index_size) , Bit#(`entry_size)) stimulus <- mkRegFileLoad("input.txt", 0, `stim_size-1);
//    let fadd <- mk_fpu_add_sub_sp_instance;
    let fadd <- (`MOD);

    FIFOF#(Tuple2#(Bit#(`FLEN), Bit#(5))) ff_golden_output <- mkSizedFIFOF(2);

    Reg#(Bit#(`index_size)) read_index <- mkReg(0);
    Reg#(Bit#(`index_size)) golden_index <- mkReg(0);

    /*doc:rule: */
    rule rl_pick_stimulus_entry;
      
      let _e = stimulus.sub(read_index);
      Bit#(8) _flags = truncate(_e);
      _e = _e >> 8;
      Bit#(`FLEN) _output = truncate(_e);
      _e = _e >> `FLEN;
      Bit#(64) _inp1 = truncate(_e);
      ff_golden_output.enq(tuple2(_output, truncate(_flags)));

//      `logLevel( tb, 0, $format("TB: Sending inputs[%d]: input : %h, output %h", read_index, _inp1,_output))

	let op3 = Exception{invalid_op : unpack(_flags[0]), divide_0 : unpack(_flags[1]), overflow : unpack(_flags[2]),underflow : unpack(_flags[3]),inexact : unpack(_flags[4])};


      Bit#(3) r_mode = pack(`rounding_mode);

      fadd.start(_inp1, 1, 0,r_mode);		//<-------------------------------------------------
      read_index <= read_index + 1;
      if(read_index == `stim_size-1)
//      if(read_index == 10)
        $finish(0);
    endrule

    /*doc:rule: */
    rule rl_check_output;
//      match {.valid,.out,.flags} = fadd.receive();
      let x = fadd.receive();
      let valid = x.valid;
      let m_out = x.value;
      let m_flags = x.ex;
      if(valid==1) begin
	      let {g_out, g_flags} = ff_golden_output.first;
	      ff_golden_output.deq;

	      Bit#(`FLEN) _out = {pack(m_out.sign), m_out.exp, m_out.sfd};

		if( _out != g_out) 
		begin
			`logLevel( tb, 0, $format("TB: Outputs mismatch[%d]. G:%h R:%h,flag %h",golden_index, g_out, m_out,m_flags))
			$finish(0);
		end

		else 
		begin
//			`logLevel( tb, 0, $format("TB: Outputs match [%d], g: %h R: %h", golden_index,g_out, m_out))
		end
		golden_index <= golden_index + 1;
	        if( g_flags != pack(m_flags)) begin
//	          `logLevel( tb, 0, $format("TB: Flags mismatch. G:%h R:%h", g_flags, m_flags))
//	          $finish(0);
	        end
      end
    endrule
  endmodule
endpackage

