  `include "Logger.bsv"
package normalize_fma;
import fpu_common    ::*;
import Vector            ::*;
import Real              ::*;
import BUtils            ::*;
import DefaultValue      ::*;
import FShow             ::*;
import GetPut            ::*;
import ClientServer      ::*;
import FIFO              ::*;
import FixedPoint        ::*;
import DReg  :: *;
`include "fpu_parameters.bsv"


/*doc:func: function for normalization after product in case of single precision.*/
function ActionValue#(Tuple3#(FloatingPoint#(8,23),Exception,Bit#(50))) normalize1_sp(Tuple2#(FloatingPoint#(8,23), Bit#(48)) operands)=actionvalue
	
	match{.din,.sfdin} = operands;
	Bit#(50) sfdiN = 0;
   FloatingPoint#(8,23) out = din;
   Bit#(2) guard = 0;
   Exception exc = defaultValue;
   //find exponent.	
   Int#(9) exp = isSubNormal(out) ? fromInteger(minexp(out)) : signExtend(unpack(unbias(out)));
   let zeros = countZerosMSB(sfdin);

	if ((zeros == 0) && (exp == fromInteger(maxexp(out)))) 
	begin
      		out.exp = maxBound - 1;
      		out.sfd = maxBound;
		sfdiN = {sfdin,2'b0};
      		guard = '1;
   	end
   	else 
	begin
      		if (zeros == 0) 
		begin
			 // carry, no sfd adjust necessary
			if (out.exp == 0)
			begin
			    	out.exp = 2;
			end
			else
			begin
			    out.exp = out.exp + 1;
			end

	 		// carry bit
	 		sfdin = sfdin << 1;
      		end
	      	else if (zeros == 1) 
		begin
			// already normalized

			if (out.exp == 0)
			out.exp = 1;

			// carry, hidden bits
			sfdin = sfdin << 2;
	      	end
	      	else if (zeros == fromInteger(valueOf(48))) 
		begin
		 	// exactly zero
			out.exp = 0;
	      	end
	      	else 
		begin
			 // try to normalize
			 Int#(9) shift = zeroExtend(unpack(pack(zeros - 1)));
			 Int#(9) maxshift = exp - fromInteger(minexp(out));

`ifdef denormal_support
			if (shift > maxshift) 
			begin
				// result will be subnormal
				sfdin = sfdin << maxshift;
				out.exp = 0;
		 	end
		 	else 
			begin
				// result will be normal
				sfdin = sfdin << shift;
				out.exp = out.exp - truncate(pack(shift));
		 	end

			 	 // carry, hidden bits
				 sfdin = sfdin << 2;
		end
`else
			if (shift <= maxshift) 
			begin
			    // result will be normal

				sfdin = sfdin << shift;
				out.exp = out.exp - truncate(pack(shift));
			end
			sfdin = sfdin << 2;
			
		 	 // carry, hidden bits
		end
`endif
      out.sfd = unpack(truncateLSB(sfdin));
      sfdiN = {1'b0,getHiddenBit(out),sfdin};
      sfdin = sfdin << fromInteger(valueOf(23));
      guard[1] = unpack(truncateLSB(sfdin));
      sfdin = sfdin << 1;
      guard[0] = |sfdin;

   end

   return tuple3(out,exc,sfdiN);
endactionvalue;









/*doc:func: function for normalization after addition in case of single precision.*/
function ActionValue#(Tuple3#(FloatingPoint#(8,23),Bit#(2),Exception)) normalize2_sp( Tuple4#(Int#(10),FloatingPoint#(8,23), Bit#(50),RoundMode) operands)
	=actionvalue
	
	match{.exp_out,.din,.sfdin,.rmode} = operands;
	let sfdiN = sfdin;
   FloatingPoint#(8,23) out = din;
   Bit#(2) guard = 0;
   Exception exc = defaultValue;
   Int#(9) exp = isSubNormal(out) ? fromInteger(minexp(out)) : signExtend(unpack(unbias(out)));
   let zeros = countZerosMSB(sfdin);

	if ((zeros == 0) && (exp == fromInteger(maxexp(out)))) 
	begin
      		out.exp = maxBound - 1;
      		out.sfd = maxBound;
      		guard = '1;
		if(rmode == Rnd_Minus_Inf || rmode == Rnd_Zero || rmode == Rnd_Plus_Inf)
			exc.overflow = True;
      		exc.inexact = True;
   	end
   	else 
	begin
      		if (zeros == 0) 
		begin
			 // carry, no sfd adjust necessary
			if (exp_out + 'd127 == 0)
			begin
			    	out.exp = 2;
				exp_out = 2;
			end
			else
			begin
				out.exp = out.exp + 1;
				exp_out = exp_out + 1;
				if(exp_out > 127) 
				begin
					exc.overflow = True;
					exc.inexact = True;
				end
			end

	 		// carry bit
	 		sfdin = sfdin << 1;
      		end
	      	else if (zeros == 1) 
		begin
			// already normalized
			if (exp_out + 'd127 == 0)
			begin
				out.exp = 1;
			end

			// carry, hidden bits
			sfdin = sfdin << 2;
			if(exp_out > 127) 
			begin
				exc.overflow = True;
				exc.inexact = True;
			end				
	      	end
	      	else if (zeros == fromInteger(50)) 
		begin
		 	// exactly zero
			out.exp = 0;
	      	end
	      	else 
		begin
			 // try to normalize
			 Int#(10) shift = zeroExtend(unpack(pack(zeros - 1)));

			 Int#(10) maxshift = exp_out + 'd126;

	`ifdef denormal_support
			if (shift > maxshift) 
			begin
				// result will be subnormal
				sfdin = sfdin << maxshift;
				out.exp = 0;
		 	end
		 	else 
			begin
				// result will be normal
				sfdin = sfdin << shift;
				out.exp = out.exp - truncate(pack(shift));
				exp_out = exp_out - shift;
				if(exp_out > 127) 
				begin
					exc.overflow = True;
					exc.inexact = True;
				end
		 	end

			 	 // carry, hidden bits
				 sfdin = sfdin << 2;
		end
`else
			if (shift <= maxshift) 
			begin
			    // result will be normal

				sfdin = sfdin << shift;
				out.exp = out.exp - truncate(pack(shift));
			end
			sfdin = sfdin << 2;
			 

		 	 // carry, hidden bits
		end
`endif
      out.sfd = unpack(truncateLSB(sfdin));
	sfdiN = sfdin;
      sfdin = sfdin << fromInteger(23);
      guard[1] = unpack(truncateLSB(sfdin));
      sfdin = sfdin << 1;

      guard[0] = |sfdin;
   end

	return tuple3(out,guard,exc);
endactionvalue;















/*doc:func: function for normalization after product in case of double precision.*/
function ActionValue#(Tuple3#(FloatingPoint#(11,52),Exception,Bit#(108))) normalize1_dp(Tuple2#(FloatingPoint#(11,52), Bit#(106)) operands)=actionvalue
	
	match{.din,.sfdin} = operands;
	Bit#(108) sfdiN = 0;
   FloatingPoint#(11,52) out = din;

   Bit#(2) guard = 0;
   Exception exc = defaultValue;
   Int#(12) exp = isSubNormal(out) ? fromInteger(minexp(out)) : signExtend(unpack(unbias(out)));
   let zeros = countZerosMSB(sfdin);

	if ((zeros == 0) && (exp == fromInteger(maxexp(out)))) 
	begin
      		out.exp = maxBound - 1;
      		out.sfd = maxBound;
		sfdiN = {sfdin,2'b0};
      		guard = '1;
   	end
   	else 
	begin
      		if (zeros == 0) 
		begin
			 // carry, no sfd adjust necessary
			if (out.exp == 0)
			begin
			    	out.exp = 2;
			end
			else
			begin
			    out.exp = out.exp + 1;
			end

	 		// carry bit
	 		sfdin = sfdin << 1;
      		end
	      	else if (zeros == 1) 
		begin
			// already normalized

			if (out.exp == 0)
			out.exp = 1;

			// carry, hidden bits
			sfdin = sfdin << 2;
	      	end
	      	else if (zeros == fromInteger(valueOf(106))) 
		begin
		 	// exactly zero
			out.exp = 0;
	      	end
	      	else 
		begin
			 // try to normalize
			 Int#(12) shift = zeroExtend(unpack(pack(zeros - 1)));
			 Int#(12) maxshift = exp - fromInteger(minexp(out));

`ifdef denormal_support
			if (shift > maxshift) 
			begin
				// result will be subnormal
				sfdin = sfdin << maxshift;
				out.exp = 0;
		 	end
		 	else 
			begin
				// result will be normal
				sfdin = sfdin << shift;
				out.exp = out.exp - truncate(pack(shift));
		 	end

			 	 // carry, hidden bits
				 sfdin = sfdin << 2;
		end
`else
			if (shift <= maxshift) 
			begin
			    // result will be normal

				sfdin = sfdin << shift;
				out.exp = out.exp - truncate(pack(shift));
			end
			sfdin = sfdin << 2;
			
		 	 // carry, hidden bits
		end
`endif
      out.sfd = unpack(truncateLSB(sfdin));
      sfdiN = {1'b0,getHiddenBit(out),sfdin};
      sfdin = sfdin << fromInteger(valueOf(52));
      guard[1] = unpack(truncateLSB(sfdin));
      sfdin = sfdin << 1;
      guard[0] = |sfdin;

   end

   return tuple3(out,exc,sfdiN);
endactionvalue;









/*doc:func: function for normalize after addition in case of double precision.*/
function ActionValue#(Tuple3#(FloatingPoint#(11,52),Bit#(2),Exception)) normalize2_dp( Tuple4#(Int#(13),FloatingPoint#(11,52), Bit#(108),RoundMode) operands)
	=actionvalue
	
	match{.exp_out,.din,.sfdin,.rmode} = operands;
	let sfdiN = sfdin;
   FloatingPoint#(11,52) out = din;
   Bit#(2) guard = 0;
   Exception exc = defaultValue;
   Int#(12) exp = isSubNormal(out) ? fromInteger(minexp(out)) : signExtend(unpack(unbias(out)));
   let zeros = countZerosMSB(sfdin);

	if ((zeros == 0) && (exp == fromInteger(maxexp(out)))) 
	begin
      		out.exp = maxBound - 1;
      		out.sfd = maxBound;
      		guard = '1;
		if(rmode == Rnd_Minus_Inf || rmode == Rnd_Zero || rmode == Rnd_Plus_Inf)
			exc.overflow = True;
      		exc.inexact = True;
   	end
   	else 
	begin
      		if (zeros == 0) 
		begin
			 // carry, no sfd adjust necessary

//			if (out.exp == 0)
			if (exp_out + 'd1023 == 0)
			begin
			    	out.exp = 2;
				exp_out = 2;
			end
			else
			begin
				out.exp = out.exp + 1;
				exp_out = exp_out + 1;
				if(exp_out > 1023) 
				begin
					exc.overflow = True;
					exc.inexact = True;
				end
			end

	 		// carry bit
	 		sfdin = sfdin << 1;
      		end
	      	else if (zeros == 1) 
		begin
			// already normalized
			if (exp_out + 'd1023 == 0)
			begin
				out.exp = 1;
			end

			// carry, hidden bits
			sfdin = sfdin << 2;
			if(exp_out > 1023) 
			begin
				exc.overflow = True;
				exc.inexact = True;
			end				
	      	end
	      	else if (zeros == fromInteger(108)) 
		begin
		 	// exactly zero
			out.exp = 0;
	      	end
	      	else 
		begin
			 // try to normalize
			 Int#(13) shift = zeroExtend(unpack(pack(zeros - 1)));

			 Int#(13) maxshift = exp_out + 'd1022;

	`ifdef denormal_support
			if (shift > maxshift) 
			begin
				// result will be subnormal
				sfdin = sfdin << maxshift;
				out.exp = 0;
		 	end
		 	else 
			begin
				// result will be normal
				sfdin = sfdin << shift;
				out.exp = out.exp - truncate(pack(shift));
				exp_out = exp_out - shift;
				if(exp_out > 1023) 
				begin
					exc.overflow = True;
					exc.inexact = True;
				end
		 	end

			 	 // carry, hidden bits
				 sfdin = sfdin << 2;
		end
`else
			if (shift <= maxshift) 
			begin
			    // result will be normal

				sfdin = sfdin << shift;
				out.exp = out.exp - truncate(pack(shift));
			end
			sfdin = sfdin << 2;
			 

		 	 // carry, hidden bits
		end
`endif
      out.sfd = unpack(truncateLSB(sfdin));
	sfdiN = sfdin;
      sfdin = sfdin << fromInteger(52);
      guard[1] = unpack(truncateLSB(sfdin));
      sfdin = sfdin << 1;

      guard[0] = |sfdin;
   end
	return tuple3(out,guard,exc);
endactionvalue;





endpackage

